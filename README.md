For running this project, testing machine must have installed Git, Node, NPM and internet browser (prefered Google Chrome)

1. Clone the git repo

2. Install global npm package for http server used for serving csv file:
    ===> npm install http-server -g

3. In your terminal/bash navigate to project location 'crealytics-frontend-challenge/assets', and then start static web server:
    ===> http-server --cors -o

    * Landing page of static web server should be shown in browser, verify that file 'products.csv' is served

4. Open another terminal/bash and navigate to root of the project 'crealytics-frontend-challenge', and run:
    ===> npm install
    ===> npm start

    * Landing page of the Product catalog search app should be shown with the message 'Please wait until database initializes!'
    * First and only first load should be around minute long
        - this is due the filling client side database with parsed CSV data and indexing documents for quick search

5. If app doesn't start, see browser console for error logs