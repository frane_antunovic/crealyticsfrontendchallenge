import { SEARCH_FIELDS, GENDERS } from './App';

var PouchDB = require('pouchdb').default;
PouchDB.plugin(require('pouchdb-quick-search'));

const csvAddress = 'http://localhost:8080/products.csv';
const regExpCommaWithoutFollowingSpace = new RegExp(',(?!\\\s)');

function validateDocument (document) {
    if (!document.hasOwnProperty('_id')) return false;
    if (!document.hasOwnProperty('title')) return false;
    if (!document.hasOwnProperty('gtin')) return false;
    if (!document.hasOwnProperty('gender')) return false;
    if (!GENDERS.includes(document.gender)) return false;
    if (!document.hasOwnProperty('sale_price')) return false;
    if (!document.hasOwnProperty('price')) return false;
    if (!document.hasOwnProperty('image_link')) return false;
    if (!document.hasOwnProperty('additional_image_link')) return false;
    if (!Array.isArray(document.additional_image_link)) return false;

    return true;
}

function parseCSV (csv) {
    let dataRows = csv.split('\n');
    let categriesRow = dataRows.splice(0, 1);

    let categories = categriesRow[0].split(',');
    let rowValues = dataRows.map(row => row.split(regExpCommaWithoutFollowingSpace));
    
    let documents = [];
    for (let i = 0; i < rowValues.length; i++) {
        let document = {};
        for (let j = 0; j < rowValues[i].length; j++) {
            if (j === 6) {
                rowValues[i][j] = rowValues[i][j].substring(1, rowValues[i][j].length - 1);
                rowValues[i][j] = rowValues[i][j].split(', ');
            }
            document[categories[j]] = rowValues[i][j];
        }
        const id = i.toString();
        document._id = id;

        if (validateDocument(document)) documents.push(document);
    }

    return documents;
}

function initDB (documents) {
    return new Promise((resolve, reject) => {
        const db = new PouchDB('products');
        db.bulkDocs(documents).then((res) => {
            console.log(res);
        }).then((res) => {
            db.search({
                fields: SEARCH_FIELDS,
                build: true
            }).then((res) => {
                console.log('Index successfuly built!', res);
                resolve();
            }).catch((err) => {
                console.log('Index building failed!');
            });
        }).catch((err) => {
            console.log(err);
        });
    });
}

function fetchCSVAndInitDB () {
    return new Promise((resolve, reject) => {
        fetch(csvAddress, {
            headers: {'content-type': 'text/plain'}
        }).then((response) => {
            return response.text();
        }).then((text) => {
            const documents = parseCSV(text);
            initDB(documents).then(res => resolve('Success!'));
        }).catch((err) => {
            console.log('Error fetching CSV file', err);
        });
    });
}

export default fetchCSVAndInitDB;