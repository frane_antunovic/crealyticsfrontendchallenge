import React, { Component } from 'react';
import { TextField, Grid, Select, MenuItem, Typography } from '@material-ui/core';

import ProductCardList from './Components/ProductCardList';
import './Styles/App.css';

var PouchDB = require('pouchdb').default;
PouchDB.plugin(require('pouchdb-quick-search'));

const PAGINATION_LIMIT = 100;
export const SEARCH_FIELDS = ['title'];
export const GENDERS = ['all', 'female', 'male', 'unisex'];

class App extends Component {
  constructor() {
    super();
    this.db = new PouchDB('products');
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleGenderSelectChange = this.handleGenderSelectChange.bind(this);
    this.state = {
      latestSearchQuery: '',
      productList: [],
      selectedGender: 'all',
      sale: false
    };
    this.timeout = null;
  }

  filterByGender (products, gender) {
    return products.filter(product => product.gender === gender);
  }

  search (query) {
    let searchOptions = {
      query: query,
      fields: SEARCH_FIELDS,
      include_docs: true,
      limit: PAGINATION_LIMIT
    };

    if (this.state.selectedGender !== 'all' || this.state.sale) {
      searchOptions.filter = this.searchFilter;
    }

    this.db.search(searchOptions).then((res) => {
      console.log(res);
      let searchResult = res.rows.map(result => result.doc);
      if (this.state.selectedGender !== 'all') {
        searchResult = this.filterByGender(searchResult, this.state.selectedGender);
      }
      this.setState({
        latestSearchQuery: query,
        productList: [...searchResult]
      });
    }).catch((err) => {
      console.log(err);
    });
  }

  handleTextChange (e) {
    let query = e.target.value;
    if (query !== '') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => this.search(query), 500);
    } else {
      this.setState({
        latestSearchQuery: '',
        productList: []
      });
    }
  }

  handleGenderSelectChange (e) {
    this.setState({
      selectedGender: e.target.value
    });
    this.search(this.state.latestSearchQuery);
  }

  render () {
    const genderSelectionList = GENDERS.map(gender => <MenuItem key={gender} value={gender}>{gender}</MenuItem>);

    return (
      <div className='App'>
        <Grid container key={'navbar'} direction='row' justify='center' alignItems="center" spacing={16} style={{height: 200, backgroundColor: '#eceff1'}}>
          <Grid item>
            <TextField
              style={{width: 500}}
              type='text'
              onChange={this.handleTextChange}
              placeholder='Search products...'
            />
          </Grid>
          <Grid item>
            <Grid container direction='row' justify='flex-start' alignItems="center" spacing={16}>
              <Grid item>
                <Typography>Gender filter: </Typography>
              </Grid>
              <Grid item>
                <Select value={this.state.selectedGender} onChange={this.handleGenderSelectChange}>
                  {genderSelectionList}
                </Select>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container key={'searchResults'} direction='row' justify='center' spacing={16} style={{backgroundColor: '#e0e0e0'}}>
          <ProductCardList 
            productList={this.state.productList}
          />
        </Grid>
      </div>
    );
  }
}

export default App;
