import React from 'react';
import ReactDOM from 'react-dom';
import './Styles/index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import fetchCSVAndInitDB from './init';

fetchCSVAndInitDB().then((result) => {
    console.log(result);
    ReactDOM.render(<App />, document.getElementById('root'));
    serviceWorker.unregister();
}).catch((err) => {
    throw new Error(err);
});

