import React from 'react';
import ProductCardComponent from './ProductCardComponent';
import { Grid } from '@material-ui/core';

function ProductCardList (props) {
    const { productList } = props;
    
    let productCardList = [];
    for (const product of productList) {
        productCardList.push(
            <Grid item xs={3} key={'gridItem_' + product._id}>
                <ProductCardComponent
                    key={'card_' + product._id}
                    title={product.title}
                    gtin={product.gtin}
                    gender={product.gender}
                    sale_price={product.sale_price}
                    price={product.price}
                    image_link={product.image_link}
                    additional_image_link={product.additional_image_link}
                />
            </Grid>
        );
    }

    if (productCardList.length > 0) {
        return productCardList;
    } else return null;
}

export default ProductCardList;
