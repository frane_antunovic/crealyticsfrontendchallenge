import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader, CardMedia, CardContent, CardActions, Collapse, IconButton, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import red from '@material-ui/core/colors/red';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';

const styles = theme => ({
    card: {
      maxWidth: 400,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    actions: {
      display: 'flex',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    }
  });

class ProductCardComponent extends React.Component {
    constructor() {
        super();
        this.state = { expanded: false };
        this.handleExpandClick = this.handleExpandClick.bind(this);
    }

    handleExpandClick = () => {
      this.setState(state => ({ expanded: !state.expanded }));
    };

    render() {
        const { classes, title, gtin, gender, sale_price, price, image_link, additional_image_link } = this.props;
        const additionalImages = additional_image_link.map(url => <CardMedia key={url} className={classes.media} image={url} title={title} style={{marginTop: 15}} />);

        return (
            <Card className={classes.card}>
                <CardHeader
                    style={{
                        height: '5vw',
                        textAlign: 'top'
                    }}
                    title={title}
                    subheader={gtin}
                />
                <CardMedia
                    className={classes.media}
                    image={image_link}
                    title={title}
                />
                <CardContent>
                    <Typography>
                        Gender: {gender} <br />
                        Price: {price} <br />
                        Sale price: {sale_price}
                    </Typography>
                </CardContent>
                <CardActions className={classes.actions} disableActionSpacing>
                    <IconButton
                        className={classnames(classes.expand, {
                        [classes.expandOpen]: this.state.expanded,
                        })}
                        onClick={this.handleExpandClick}
                        aria-expanded={this.state.expanded}
                        aria-label='Show more'
                    >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
                <Collapse in={this.state.expanded} timeout='auto' unmountOnExit>
                    <CardContent>
                        {additionalImages}
                    </CardContent>
                </Collapse>
            </Card>
        );
    }
}

ProductCardComponent.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    gtin: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    sale_price: PropTypes.string,
    price: PropTypes.string.isRequired,
    image_link: PropTypes.string.isRequired,
    additional_image_link: PropTypes.array
};
  
export default withStyles(styles)(ProductCardComponent);